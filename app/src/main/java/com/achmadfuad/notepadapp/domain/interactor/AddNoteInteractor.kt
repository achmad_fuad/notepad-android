package com.achmadfuad.notepadapp.domain.interactor

import com.achmadfuad.framework.FlowUseCase
import com.achmadfuad.framework.Resource
import com.achmadfuad.notepadapp.data.repository.NoteRepository
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity
import kotlinx.coroutines.flow.Flow

class AddNoteInteractor(
    private val repo: NoteRepository
) : FlowUseCase<NoteEntity, Resource>() {

    override fun execute(parameters: NoteEntity?): Flow<Resource> {
        return repo.addNote(parameters)
    }
}
