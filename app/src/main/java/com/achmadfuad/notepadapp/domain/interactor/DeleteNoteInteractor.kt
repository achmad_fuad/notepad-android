package com.achmadfuad.notepadapp.domain.interactor

import com.achmadfuad.framework.FlowUseCase
import com.achmadfuad.framework.Resource
import com.achmadfuad.notepadapp.data.repository.NoteRepository
import kotlinx.coroutines.flow.Flow

class DeleteNoteInteractor(
    private val repo: NoteRepository
) : FlowUseCase<Int, Resource>() {

    override fun execute(parameters: Int?): Flow<Resource> {
        return repo.deleteNotes(parameters)
    }
}
