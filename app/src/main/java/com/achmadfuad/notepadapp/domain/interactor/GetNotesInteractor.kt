package com.achmadfuad.notepadapp.domain.interactor

import com.achmadfuad.framework.FlowUseCase
import com.achmadfuad.framework.Resource
import com.achmadfuad.notepadapp.data.repository.NoteRepository
import kotlinx.coroutines.flow.Flow

class GetNotesInteractor(private val repo: NoteRepository
) : FlowUseCase<Nothing?, Resource>() {

    override fun execute(parameters: Nothing?): Flow<Resource> {
        return repo.getNotes()
    }
}