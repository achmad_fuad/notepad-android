package com.achmadfuad.notepadapp.presentation.editor

import androidx.lifecycle.MutableLiveData
import com.achmadfuad.framework.BaseViewModel
import com.achmadfuad.framework.Resource
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity
import com.achmadfuad.notepadapp.domain.interactor.AddNoteInteractor
import com.achmadfuad.notepadapp.domain.interactor.DeleteNoteInteractor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

class EditorViewModel(private val addNoteInteractor: AddNoteInteractor,
                      private val deleteNoteInteractor: DeleteNoteInteractor) : BaseViewModel() {
    val addNotesResponse = MutableLiveData<Resource>()
    val deleteNotesResponse = MutableLiveData<Resource>()
    val bTextId = MutableLiveData<Int?>(null)
    val bTextTitle = MutableLiveData("")

    val bTextTitleError = MutableLiveData<String>()
    val bIsEdit = MutableLiveData(false)
    val bIsBold = MutableLiveData(false)
    val bIsItalic = MutableLiveData(false)
    val bIsUnderline = MutableLiveData(false)
    val bIsBullet = MutableLiveData(false)
    val bIsNumber = MutableLiveData(false)

    fun addNotesToDatabase(descContent: String) {
        val noteRequestBody = NoteEntity(
            bTextId.value ?: Random().nextInt(10000 - 1 + 1) + 1,
            bTextTitle.value,
            descContent
        )
        launch {
            addNoteInteractor(noteRequestBody).collect {
                addNotesResponse.postValue(it)
            }
        }
    }

    fun deleteNotes() {
        launch {
            deleteNoteInteractor(bTextId.value).collect {
                deleteNotesResponse.postValue(it)
            }
        }
    }
}