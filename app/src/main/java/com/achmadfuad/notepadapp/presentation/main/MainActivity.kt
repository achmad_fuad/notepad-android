package com.achmadfuad.notepadapp.presentation.main

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.achmadfuad.framework.BaseActivity
import com.achmadfuad.framework.Resource
import com.achmadfuad.framework.ViewDataBindingOwner
import com.achmadfuad.framework.observe
import com.achmadfuad.notepadapp.R
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity
import com.achmadfuad.notepadapp.databinding.ActivityMainBinding
import com.achmadfuad.notepadapp.presentation.editor.EditorActivity
import com.achmadfuad.notepadapp.presentation.main.adapter.NotesItemAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<MainViewModel>(),
    MainView,
    ViewDataBindingOwner<ActivityMainBinding> {

    override var binding: ActivityMainBinding? = null
    override var notesItemAdapter = NotesItemAdapter()
    override var notesListLayoutManager = LinearLayoutManager(this)

    override val layoutResourceId = R.layout.activity_main
    override val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        observeResult()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAllNotes()
    }

    private fun initUI() {
        title = resources.getString(R.string.title_list_note)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        val dividerItemDecoration =
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        binding?.rvNotes?.addItemDecoration(dividerItemDecoration)
    }

    private fun observeResult() {
        observe(viewModel.notesResponse) { result ->
            result?.let {
                when (result) {
                    is Resource.Success<*> -> {
                        val data = result.data as List<NoteEntity>
                        notesItemAdapter.setData(data)
                        viewModel.isDataAvailable.value = !data.isNullOrEmpty()
                    }
                }
            }
        }
    }

    override fun onClickAddNotes(view: View) {
        EditorActivity.startThisActivity(this)
    }
}