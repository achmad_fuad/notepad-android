package com.achmadfuad.notepadapp.presentation.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.achmadfuad.framework.BaseRecycleViewAdapter
import com.achmadfuad.framework.BaseViewHolder
import com.achmadfuad.framework.ViewDataBindingOwner
import com.achmadfuad.notepadapp.R
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity
import com.achmadfuad.notepadapp.databinding.ItemNotesBinding
import com.achmadfuad.notepadapp.presentation.editor.EditorActivity

class NotesItemAdapter : BaseRecycleViewAdapter<NoteEntity>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<NoteEntity> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ListViewHolder(parent.context, view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<NoteEntity>, position: Int) {
        holder.bindData(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_notes
    }

    class ListViewHolder(context: Context, view: View) :
        NotesItemView,
        BaseViewHolder<NoteEntity>(context, view),
        ViewDataBindingOwner<ItemNotesBinding> {

        override var binding: ItemNotesBinding? = null
        private lateinit var data: NoteEntity
        var viewModel: NotesItemViewModel? = null

        init {
            binding?.vm = NotesItemViewModel()
            binding?.view = this
            viewModel = binding?.vm

        }

        override fun bindData(data: NoteEntity) {
            this.data = data
            viewModel?.resetData()
            viewModel?.setData(data)

        }

        override fun onClickItem(view: View) {
            EditorActivity.startThisActivity(context, data)
        }

        override fun onClickRemove(view: View) {
        }
    }
}