package com.achmadfuad.notepadapp.presentation.editor

import android.view.View
import com.achmadfuad.framework.LifecycleView

interface EditorView: LifecycleView {
    fun onClickBold (view: View)
    fun onClickItalic (view: View)
    fun onClickUnderline (view: View)
    fun onClickOrderedList (view: View)
    fun onClickUnorderedList (view: View)

    fun onClickSave (view: View)
    fun onClickDelete (view: View)
}