package com.achmadfuad.notepadapp.presentation.main.adapter

import android.view.View

interface NotesItemView {
    fun onClickItem(view: View)
    fun onClickRemove(view: View)
}