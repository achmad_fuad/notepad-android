package com.achmadfuad.notepadapp.presentation.main.adapter

import androidx.databinding.ObservableField
import com.achmadfuad.framework.BaseViewModel
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity

class NotesItemViewModel : BaseViewModel() {

    var bTitle = ObservableField<String>()

    fun resetData() {
        bTitle.set(null)
    }

    fun setData(data: NoteEntity) {
        bTitle.set(data.title)
    }
}