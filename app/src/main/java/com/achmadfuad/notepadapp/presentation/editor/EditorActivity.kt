package com.achmadfuad.notepadapp.presentation.editor

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.achmadfuad.framework.BaseActivity
import com.achmadfuad.framework.Resource
import com.achmadfuad.framework.ViewDataBindingOwner
import com.achmadfuad.framework.observe
import com.achmadfuad.notepadapp.R
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity
import com.achmadfuad.notepadapp.databinding.ActivityEditorBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditorActivity : BaseActivity<EditorViewModel>(),
    EditorView, ViewDataBindingOwner<ActivityEditorBinding> {

    override val layoutResourceId = R.layout.activity_editor
    override val viewModel: EditorViewModel by viewModel()
    override var binding: ActivityEditorBinding? = null

    companion object {
        private const val BUNDLE_DATA = "notes_data"
        fun startThisActivity(context: Context, data: NoteEntity? = null) {
            val intent = Intent(context, EditorActivity::class.java)

            data?.let {
                intent.putExtra(BUNDLE_DATA, it)
            }
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getIntentData()
        initUI()
        observeResponse()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.itemId.let {
            when (it) {
                android.R.id.home -> onBackPressed()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUI() {
        viewModel.bIsEdit.postValue(intent.getSerializableExtra(BUNDLE_DATA) != null)
        title = resources.getString(R.string.title_note_editor)
    }

    private fun getIntentData() {
        val source = intent.getSerializableExtra(BUNDLE_DATA)
        source?.let {
            it as NoteEntity
            viewModel.bTextId.value = it.id
            viewModel.bTextTitle.value = it.title
            binding?.textContent?.setText(Html.fromHtml(it.desc))
        }
    }

    private fun observeResponse() {
        observe(viewModel.addNotesResponse, ::manageAddNotesResponse)
        observe(viewModel.deleteNotesResponse, ::manageDeleteNotesResponse)
    }

    private fun manageAddNotesResponse(result: Resource) {
        when (result) {
            is Resource.Success<*> -> {
                val data = result.data as String
                Toast.makeText(this, data, Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    private fun manageDeleteNotesResponse(result: Resource) {
        when (result) {
            is Resource.Success<*> -> {
                val data = result.data as String
                Toast.makeText(this, data, Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    override fun onClickBold(view: View) {
        viewModel.bIsBold.value = !viewModel.bIsBold.value!!
    }

    override fun onClickItalic(view: View) {
        viewModel.bIsItalic.value = !viewModel.bIsItalic.value!!
    }

    override fun onClickUnderline(view: View) {
        viewModel.bIsUnderline.value = !viewModel.bIsUnderline.value!!
    }

    override fun onClickOrderedList(view: View) {
        viewModel.bIsNumber.value = !viewModel.bIsNumber.value!!
        viewModel.bIsBullet.value = false
    }

    override fun onClickUnorderedList(view: View) {
        viewModel.bIsBullet.value = !viewModel.bIsBullet.value!!
        viewModel.bIsNumber.value = false

        if (viewModel.bIsBullet.value!!) {
            val pos: Int = binding?.textContent?.text?.length!!
            binding?.textContent?.apply {
                setIsBullet(true)
                setSelection(pos)
                requestFocus()
                append("\n")
            }
        }
    }

    override fun onClickSave(view: View) {
        var isValid = true

        viewModel.bTextTitleError.value = null
        if (TextUtils.isEmpty(viewModel.bTextTitle.value)) {
            isValid = false
            viewModel.bTextTitleError.value = getString(R.string.text_error_empty_title)
        }

        if (isValid) {
            val descContent = Html.toHtml(binding?.textContent?.text)
            viewModel.addNotesToDatabase(descContent)
        }
    }

    override fun onClickDelete(view: View) {
        viewModel.deleteNotes()
    }
}