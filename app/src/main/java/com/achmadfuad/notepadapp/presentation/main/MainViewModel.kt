package com.achmadfuad.notepadapp.presentation.main

import androidx.lifecycle.MutableLiveData
import com.achmadfuad.framework.BaseViewModel
import com.achmadfuad.framework.Resource
import com.achmadfuad.notepadapp.domain.interactor.GetNotesInteractor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainViewModel(private val getNotesInteractor: GetNotesInteractor) : BaseViewModel() {

    val notesResponse = MutableLiveData<Resource>()
    val isDataAvailable = MutableLiveData(false)

    fun getAllNotes() {
        launch {
            getNotesInteractor().collect {
                notesResponse.postValue(it)
            }
        }
    }
}