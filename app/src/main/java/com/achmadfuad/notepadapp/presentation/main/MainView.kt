package com.achmadfuad.notepadapp.presentation.main

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.achmadfuad.framework.LifecycleView
import com.achmadfuad.notepadapp.presentation.main.adapter.NotesItemAdapter

interface MainView: LifecycleView {
    fun onClickAddNotes(view: View)

    var notesItemAdapter: NotesItemAdapter
    var notesListLayoutManager: LinearLayoutManager
}