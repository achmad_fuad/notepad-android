package com.achmadfuad.notepadapp.presentation.utils

import android.content.Context
import android.graphics.Typeface
import android.text.Editable
import android.text.Selection
import android.text.Spannable
import android.text.TextWatcher
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.util.AttributeSet
import com.achmadfuad.notepadapp.R

class CustomRichText @JvmOverloads constructor(context: Context,
                                               attrs: AttributeSet? = null) :
    androidx.appcompat.widget.AppCompatEditText(context, attrs, androidx.appcompat.R.attr.editTextStyle), TextWatcher {

    private var mIsBullet = false
    private var mIsNumber = false
    private var mIsBold = false
    private var mIsItalic = false
    private var mIsUnderline = false
    private var mStyleStart = 0
    private var mPreviousTextLength = 0

    init {
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.CustomRichText, androidx.appcompat.R.attr.editTextStyle, 0)
        try {
            this.addTextChangedListener(this)
            mIsBold = attributes.getBoolean(R.styleable.CustomRichText_isBold, false)
            mIsItalic = attributes.getBoolean(R.styleable.CustomRichText_isItalic, false)
            mIsUnderline = attributes.getBoolean(R.styleable.CustomRichText_isUnderline, false)
            mIsNumber = attributes.getBoolean(R.styleable.CustomRichText_isNumber, false)
            mIsBullet = attributes.getBoolean(R.styleable.CustomRichText_IsBullet, false)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun handleFormat(s: Editable, position: Int, format: Any) {
        try {
            val ss = s.getSpans(
                mStyleStart, position,
                StyleSpan::class.java
            )
            for (i in ss.indices) {
                if (ss[i].style == format) {
                    s.removeSpan(ss[i])
                }
            }
            s.setSpan(format, mStyleStart, position, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun afterTextChanged(s: Editable) {
        val mBackSpace: Boolean = mPreviousTextLength >= s.length

        if ((mIsBullet || mIsNumber) && !mBackSpace && s.toString().endsWith("\n")) {
            this.append("\u2022")
        }

        val position = Selection.getSelectionStart(this.text)
        //handle bold
        if (!mBackSpace && mIsBold) {
            handleFormat(s, position, StyleSpan(Typeface.BOLD))
        }
        if (!mBackSpace && mIsItalic) {
            handleFormat(s, position, StyleSpan(Typeface.ITALIC))
        }
        if (!mBackSpace && mIsUnderline) {
            handleFormat(s, position, UnderlineSpan())
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        mStyleStart = start
        mPreviousTextLength = s.length
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        //unused
    }

    fun setIsBold(isBold: Boolean) {
        mIsBold = isBold
    }

    fun setIsItalic(isItalic: Boolean) {
        mIsItalic = isItalic
    }

    fun setIsUnderline(isUnderline: Boolean) {
        mIsUnderline = isUnderline
    }

    fun setIsBullet(isBullet: Boolean) {
        mIsBullet = isBullet
    }

    fun setIsNumber(isNumber: Boolean) {
        mIsNumber = isNumber
    }
}