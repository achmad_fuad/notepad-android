package com.achmadfuad.notepadapp.application

import androidx.multidex.MultiDexApplication
import com.achmadfuad.notepadapp.module.interactorModule
import com.achmadfuad.notepadapp.module.repositoryModule
import com.achmadfuad.notepadapp.module.roomModule
import com.achmadfuad.notepadapp.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class BaseApplication : MultiDexApplication(){

    override fun onCreate() {
        super.onCreate()

        setupKoin()
    }

    private fun setupKoin(){
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@BaseApplication)
            modules(listOf(
                viewModelModule,
                interactorModule,
                repositoryModule,
                roomModule
                ))
        }
    }
}