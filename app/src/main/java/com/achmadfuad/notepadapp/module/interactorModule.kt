package com.achmadfuad.notepadapp.module

import com.achmadfuad.notepadapp.domain.interactor.AddNoteInteractor
import com.achmadfuad.notepadapp.domain.interactor.DeleteNoteInteractor
import com.achmadfuad.notepadapp.domain.interactor.GetNotesInteractor
import org.koin.dsl.module

val interactorModule =  module {
    single { AddNoteInteractor(get()) }
    single { GetNotesInteractor(get()) }
    single { DeleteNoteInteractor(get()) }
}