package com.achmadfuad.notepadapp.module

import androidx.room.Room
import com.achmadfuad.notepadapp.data.room.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val roomModule = module {
    single<AppDatabase> {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "database")
            .build()
    }

    single { get<AppDatabase>().notesDao() }
}