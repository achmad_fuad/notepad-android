package com.achmadfuad.notepadapp.module

import com.achmadfuad.notepadapp.data.repository.NoteRepository
import com.achmadfuad.notepadapp.data.repository.NoteRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<NoteRepository> { NoteRepositoryImpl(roomNoteService = get()) }
}