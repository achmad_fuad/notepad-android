package com.achmadfuad.notepadapp.module


import com.achmadfuad.notepadapp.presentation.editor.EditorViewModel
import com.achmadfuad.notepadapp.presentation.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(getNotesInteractor = get()) }
    viewModel { EditorViewModel(get(), get()) }
}