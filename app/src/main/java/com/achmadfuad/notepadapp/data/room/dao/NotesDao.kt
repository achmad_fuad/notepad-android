package com.achmadfuad.notepadapp.data.room.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity

@Dao
interface NotesDao {
    @Query("SELECT * FROM NoteTable")
    fun getAll() : List<NoteEntity>

    @Insert(onConflict = REPLACE)
    fun addNote(vararg notes : NoteEntity)

    @Query("DELETE FROM NoteTable WHERE id = :id")
    fun delete(id: Int)
}