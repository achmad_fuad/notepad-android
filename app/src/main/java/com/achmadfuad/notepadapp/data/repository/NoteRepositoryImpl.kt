package com.achmadfuad.notepadapp.data.repository

import com.achmadfuad.framework.Resource
import com.achmadfuad.notepadapp.data.room.AppDatabase
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class NoteRepositoryImpl(
    private val roomNoteService: AppDatabase
) : NoteRepository {
    override fun addNote(bodyRequest: NoteEntity?): Flow<Resource> {
        return flow {
            val data = roomNoteService.notesDao().addNote(bodyRequest!!)
            emit(Resource.Success("Success adding notes"))
        }
    }

    override fun getNotes(): Flow<Resource> {
        return flow {
            emit(
                Resource.Success(roomNoteService.notesDao().getAll())
            )
        }
    }

    override fun deleteNotes(bodyRequest: Int?): Flow<Resource> {
        return flow {
            val data = roomNoteService.notesDao().delete(bodyRequest!!)
            emit(Resource.Success("Success remove notes"))
        }
    }
}