package com.achmadfuad.notepadapp.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.achmadfuad.notepadapp.data.room.dao.NotesDao
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity

@Database(entities = [NoteEntity::class],version = 1)
abstract class AppDatabase : RoomDatabase() {
     abstract fun notesDao(): NotesDao
}