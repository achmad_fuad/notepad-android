package com.achmadfuad.notepadapp.data.repository

import com.achmadfuad.framework.Resource
import com.achmadfuad.notepadapp.data.room.entity.NoteEntity
import kotlinx.coroutines.flow.Flow

interface NoteRepository {
    @Throws(Exception::class)
    fun addNote(bodyRequest: NoteEntity?): Flow<Resource>

    @Throws(Exception::class)
    fun getNotes(): Flow<Resource>

    @Throws(Exception::class)
    fun deleteNotes(bodyRequest: Int?): Flow<Resource>
}