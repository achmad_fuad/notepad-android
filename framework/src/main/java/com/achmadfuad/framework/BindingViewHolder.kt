package com.achmadfuad.framework

import android.view.View

interface BindingViewHolder<in T> {
    fun onItemClick(view: View, item: T)
}
