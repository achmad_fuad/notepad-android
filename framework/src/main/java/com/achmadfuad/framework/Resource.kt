package com.achmadfuad.framework

sealed class Resource {

    data class Success<T>(val data: T?) : Resource()
}
