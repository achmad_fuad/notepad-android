package com.achmadfuad.framework

import android.content.Context
import android.view.View

abstract class BaseViewHolder<T>(val context: Context, view: View) :
    androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    init {
        if (this is ViewDataBindingOwner<*>) {
            setViewBinding(view)
            binding?.executePendingBindings()
            if (this is BindingViewHolder<*>) {
                binding?.setVariable(BR.view, this)
            }
        }
    }

    abstract fun bindData(data: T)
}
