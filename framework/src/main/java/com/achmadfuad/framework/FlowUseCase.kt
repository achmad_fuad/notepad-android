package com.achmadfuad.framework

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch

abstract class FlowUseCase<in P, R>() {
    operator fun invoke(parameters: P? = null): Flow<R> = execute(parameters)
        .catch { e ->
         Exception()
        }

    protected abstract fun execute(parameters: P? = null): Flow<R>
}
