package com.achmadfuad.framework

import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    abstract val layoutResourceId: Int
    abstract val viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setLayoutIfDefined()
    }

    private fun setLayoutIfDefined() {
        if (this is ViewDataBindingOwner<*>) {
            setContentViewBinding(this, layoutResourceId)
            binding?.setVariable(BR.vm, viewModel)
            binding?.lifecycleOwner = this
            if (this is LifecycleView) {
                binding?.setVariable(BR.view, this)
            }
        } else {
            setContentView(layoutResourceId)
        }
    }

    protected fun setHomeAsUpIndicator(@DrawableRes resId: Int) {
        supportActionBar?.setHomeAsUpIndicator(resId)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this is ViewDataBindingOwner<*>) {
            clearDataBinding()
        }
    }
}